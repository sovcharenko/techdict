<?php

namespace app\models\DL;

use Yii;

/**
 * This is the model class for table "options".
 *
 * @property integer $id
 * @property string $key
 * @property string $title
 * @property string $value
 */
class Options extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'options';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value'], 'string'],
            [['key'], 'string', 'max' => 30],
            [['title'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'key' => Yii::t('app/models', 'Key'),
            'title' => Yii::t('app/models', 'Title'),
            'value' => Yii::t('app/models', 'Value'),
        ];
    }
}
