<?php

namespace app\models\DL\behaviours;

use Yii;
use yii\db\ActiveRecord;
use yii\base\Behavior;

class RBACBehaviour extends Behavior
{
    /**
     * @var \app\models\DL\User
     */
    protected $user;

    /**
     * @var \yii\rbac\Role
     */
    protected $role;

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'addRBACUser',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'updateRBACUser',
            ActiveRecord::EVENT_BEFORE_DELETE => 'deleteRBACUser',
        ];
    }

    /**
     * Assign user to a roles
     *
     * @param $event
     */
    public function addRBACUser($event)
    {
        $this->initEventUserAndRole( $event );
        Yii::$app->getAuthManager()->assign( $this->role, $this->user->id );
    }

    /**
     * Revokes all roles from a user and assigns new role to a user
     *
     * @param $event
     */
    public function updateRBACUser($event)
    {
        $this->initEventUserAndRole( $event );
        Yii::$app->getAuthManager()->revokeAll( $this->user->id );
        Yii::$app->getAuthManager()->assign( $this->role, $this->user->id );
    }

    /**
     * Revokes all roles from a user
     *
     * @param $event
     */
    public function deleteRBACUser($event)
    {
        $this->initEventUserAndRole( $event );
        Yii::$app->getAuthManager()->revokeAll( $this->user->id );
    }

    /**
     * Sets the values of this behaviour props and validates current role according to current RBAC
     *
     * @param $event
     */
    protected function initEventUserAndRole( $event )
    {
        $this->user = $event->sender;
        $this->role = Yii::$app->getAuthManager()->getRole( $this->user->role );
        if (!$this->role) {
            $this->user->addError('role', Yii::t('app/models', 'Role "{0}" does not exist in the system', $this->user->role));
        }
    }
}