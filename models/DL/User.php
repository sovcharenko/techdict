<?php

namespace app\models\DL;

use Yii;
use app\models\DL\behaviours\RBACBehaviour;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $auth_key
 * @property string $access_token
 * @property string $role
 *
 * @property Dictionary[] $dictionaries
 */
class User extends ActiveRecord implements IdentityInterface
{
    /**
     * @var string Default password
     */
    private static $password_default = '123456';

    /**
     * @return array Roles and their labels
     */
    public static function roles()
    {
        return [
            'admin' => Yii::t('app/models', 'Role Administrator'),
            'user' => Yii::t('app/models', 'Role Regular User'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // -- Validation
            [['username', 'password', 'role'], 'required'],
            [['username'], 'string', 'max' => 45],
            [['username'], 'unique'],
            [['password', 'auth_key', 'access_token'], 'string', 'max' => 100],
            [['password'], 'string', 'max' => 100, 'min' => 5,],
            [['role'], 'string', 'max' => 10],
            [['role'], 'in', 'range' => array_keys( self::roles() )], // Created RBAC roles
            // -- Default values
            ['role', 'default', 'value' => 'user'],
            ['password', 'default', 'value' => Yii::$app->security->generatePasswordHash( self::$password_default ),
                'when' => function($model) { return $model->isNewRecord; }],
            ['auth_key', 'default', 'value' => Yii::$app->security->generateRandomString(),
                'when' => function($model) { return $model->isNewRecord; }],
            ['access_token', 'default', 'value' => Yii::$app->security->generateRandomString(),
                'when' => function($model) { return $model->isNewRecord; }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                RBACBehaviour::className()
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'username' => Yii::t('app/models', 'Username'),
            'password' => Yii::t('app/models', 'Password'),
            'auth_key' => Yii::t('app/models', 'Auth Key'),
            'access_token' => Yii::t('app/models', 'Access Token'),
            'role' => Yii::t('app/models', 'Role'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDictionaries()
    {
        return $this->hasMany(Dictionary::className(), ['user_id' => 'id']);
    }

    /**
     * *****************************************************************************************************************
     * Identity Interface
     * *****************************************************************************************************************
     */

    /**
     * @inheritDoc
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * @inheritDoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::find()->where(['access_token' => $token])->one();
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritDoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key == $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::find()->where(['username' => $username])->one();
    }

}
