<?php

namespace app\models\DL;

use Yii;

/**
 * This is the model class for table "dictionary".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $description
 * @property integer $is_public
 *
 * @property User $user
 * @property DictionaryHasWord[] $dictionaryHasWords
 */
class Dictionary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dictionary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'is_public'], 'integer'],
            [['title', 'description'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'user_id' => Yii::t('app/models', 'User ID'),
            'title' => Yii::t('app/models', 'Title'),
            'description' => Yii::t('app/models', 'Description'),
            'is_public' => Yii::t('app/models', 'Is Public'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDictionaryHasWords()
    {
        return $this->hasMany(DictionaryHasWord::className(), ['dictionary_id' => 'id']);
    }
}
