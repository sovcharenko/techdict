<?php

namespace app\models\DL;

use Yii;

/**
 * This is the model class for table "dictionary_has_word".
 *
 * @property integer $id
 * @property integer $dictionary_id
 * @property integer $word_id
 * @property string $user_note
 *
 * @property Dictionary $dictionary
 * @property Word $word
 */
class DictionaryHasWord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dictionary_has_word';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dictionary_id', 'word_id'], 'integer'],
            [['user_note'], 'string'],
            [['dictionary_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dictionary::className(), 'targetAttribute' => ['dictionary_id' => 'id']],
            [['word_id'], 'exist', 'skipOnError' => true, 'targetClass' => Word::className(), 'targetAttribute' => ['word_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'dictionary_id' => Yii::t('app/models', 'Dictionary ID'),
            'word_id' => Yii::t('app/models', 'Word ID'),
            'user_note' => Yii::t('app/models', 'User Note'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDictionary()
    {
        return $this->hasOne(Dictionary::className(), ['id' => 'dictionary_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWord()
    {
        return $this->hasOne(Word::className(), ['id' => 'word_id']);
    }
}
