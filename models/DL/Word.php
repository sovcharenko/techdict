<?php

namespace app\models\DL;

use Yii;

/**
 * This is the model class for table "word".
 *
 * @property integer $id
 * @property string $word
 * @property string $source_language
 * @property string $translations
 * @property string $meaning_links
 * @property integer $is_public
 *
 * @property DictionaryHasWord[] $dictionaryHasWords
 */
class Word extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'word';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['word', 'translations', 'meaning_links'], 'string'],
            [['is_public'], 'integer'],
            [['source_language'], 'string', 'max' => 120],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/models', 'ID'),
            'word' => Yii::t('app/models', 'Word'),
            'source_language' => Yii::t('app/models', 'Source Language'),
            'translations' => Yii::t('app/models', 'Translations'),
            'meaning_links' => Yii::t('app/models', 'Meaning Links'),
            'is_public' => Yii::t('app/models', 'Is Public'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDictionaryHasWords()
    {
        return $this->hasMany(DictionaryHasWord::className(), ['word_id' => 'id']);
    }
}
