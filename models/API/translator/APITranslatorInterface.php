<?php

namespace app\APITranslator;

interface APITranslatorInterface
{
    /**
     * @return array Of available languages in a current parser
     * Array of available languages in a current parser in format of key-value pairs,
     * where key is FROM language and value is TO language
     */
    public function getAvailableLanguages();

    /**
     * Checks if translation available for the pair of languages
     * @param $targetLanguage
     * @param $sourceLanguage
     * @return bool
     */
    public function isTranslationAvailable( $sourceLanguage, $targetLanguage );

    /**
     * @param string $sourceLanguage
     * Language of translation
     * @return array
     * Array of available translation languages for the $sourceLanguage, or the array of
     * available languages in format of key-value pairs where key is $sourceLanguage
     * and value is TO language. Also array could contain language codes ans names
     */
    public function getAvailableLanguagesFor( $sourceLanguage );

    /**
     * @param string $word
     * Source word or phrase
     * @param string $targetLanguage
     * Translation language
     * @param string $sourceLanguage
     * Source language of a source word or phrase. Could be auto detected
     * @return string|array
     * Translation of a source word or phrase
     */
    public function getTranslation( $word, $targetLanguage, $sourceLanguage );

    /**
     * @param $word
     * Source word or phrase
     * @param array $targetLanguages
     * Translation languages
     * @param string $sourceLanguage
     * Source language of a source word or phrase. Could be auto detected
     * @return array
     * Array of a key-value pairs, where key is a translation language and value is a
     * translation of a source word or phrase
     */
    public function getTranslations( $word, $targetLanguages, $sourceLanguage );

    /**
     * @param $word
     * Source word or phrase
     * @return string|array
     * Source language of a source word or phrase
     */
    public function detectLanguage( $word );
}