<?php

namespace app\APITranslatorAdapters;

use Yii;
use app\APITranslator\APIAbstractTranslator;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\httpclient\Exception as ClientException;
use yii\base\Exception;

class YandexTranslator extends APIAbstractTranslator
{
    /**
     * Caching keys and storing duration for the internal data
     */
    const CACHE_KEY_LANGUAGES = 'Yandex_Translator_API_LANGUAGES';
    const CACHE_DURATION_LANGUAGES = 3600 * 24 * 7 * 3; // 3 weeks
    const CACHE_KEY_TRANSLATE = 'Yandex_Translator_API_TRANSLATE_';
    const CACHE_DURATION_TRANSLATE = 3600 * 24 * 7; // 1 week

    /**
     * Partial URLs for requests to the Yandex Translator API
     */
    const URL_GET_LANGUAGES = 'getLangs';
    const URL_DETECT_LANGUAGE = 'detect';
    const URL_TRANSLATE = 'translate';

    /**
     * @inheritDoc
     */
    public $baseUrl = 'https://translate.yandex.net/api/v1.5/tr.json/';

    /**
     * @inheritDoc
     */
    public $APIKeys = [
        'trnsl.1.1.20170411T125352Z.093e7d37e00dbbf2.7a0f8575a884c67228a435b031508c66ddc44296'
    ];

    /**
     * @inheritDoc
     */
    public $requestConfig = [
        'format' => Client::FORMAT_URLENCODED
    ];

    /**
     * @inheritDoc
     */
    protected $isLanguageAutoDetectAvailable = true;

    /**
     * @var array Error odes of Yandex translator API
     */
    private $APIErrorsCodes = ['404', '401', '402'];

    /**
     * Set Api key if you want to use API key from external source (like DB)
     *
     * @param string $ApiKey
     */
    public function setApiKey( $ApiKey )
    {
        $this->APIKeys[] = $ApiKey;
    }

    /**
     * @inheritDoc
     */
    public function getAvailableLanguages()
    {
        return $this->getLanguages();
    }

    /**
     * @inheritDoc
     */
    public function getAvailableLanguagesFor($sourceLanguage)
    {
        return $this->getLanguages();
    }

    /**
     * @inheritDoc
     */
    public function isTranslationAvailable($sourceLanguage, $targetLanguage)
    {
        $sourceLanguage = $this->getLanguage($sourceLanguage);
        $targetLanguage = $this->getLanguage($targetLanguage);
        return !empty($sourceLanguage) && !empty($targetLanguage);
    }

    /**
     * @inheritDoc
     */
    public function getTranslation($word, $targetLanguage, $sourceLanguage)
    {
        return empty($word) ? false : $this->getWordTranslate($word, $targetLanguage, $sourceLanguage);
    }

    /**
     * @inheritDoc
     */
    public function getTranslations($word, $targetLanguages, $sourceLanguage)
    {
        if ( empty($word) ) return false;
        $translatedText = [];

        foreach ($targetLanguages as $targetLanguage) {
            $translatedText[] = $this->getWordTranslate($word, $targetLanguage, $sourceLanguage);
        }
        return $translatedText;
    }

    /**
     * @inheritDoc
     */
    public function detectLanguage($word)
    {
        $langCode = $this->getResponse(self::URL_DETECT_LANGUAGE, ['text' => $word]);
        if ( !empty($langCode) ) {
            $detectedLanguage = $this->getLanguage( $langCode['lang'] );
            return empty($detectedLanguage) ? $langCode['lang'] : $detectedLanguage;
        } else {
            return false;
        }
    }

    /**
     * *****************************************************************************************************************
     * Internal methods
     * *****************************************************************************************************************
     */

    /**
     * @param string $partialUrl
     * Part of the URL for the request, it will be joined with base URL
     * @param array $requestData
     * Array of request data, it will be joined with API key
     * @return array|\yii\httpclient\Response->data
     * Returns array - parsed JSON response
     * @throws ClientException
     * @throws Exception
     * When the network is unavailable or response has errors (for example, wrong API key)
     */
    private function getResponse($partialUrl, $requestData)
    {
        $response = [];
        $isResponseSuccessful = true;

        // -- Trying every API Key until request will be successful
        foreach($this->APIKeys as $APIKey) {
            // -- Request
            $response = [];
            try {
                $response = $this->client->createRequest()
                    ->setUrl( $this->baseUrl . $partialUrl )
                    ->setMethod( 'POST' )
                    ->setData(
                        ArrayHelper::merge(
                            ['key' => $APIKey],
                            $requestData
                        )
                    )->send();
            } catch (ClientException $clientException) {
                Yii::warning(
                    $clientException->getCode() . $clientException->getMessage() . PHP_EOL .
                    $clientException->getTraceAsString() . PHP_EOL
                );
                throw new Exception( $this->EXCEPTION_MESSAGE );
            }
            // -- Check response codes
            if (
                in_array($response->getStatusCode(), $this->APIErrorsCodes) ||
                in_array($response->data['code'], $this->APIErrorsCodes) ||
                empty($response)
            ) {
                Yii::warning( $this::className() . " {$response->data['code']} {$response->data['message']}");
                $isResponseSuccessful = false;
            } else {
                $isResponseSuccessful = true;
                break;
            }
        }

        // -- Trow new Exception if request was unsuccessful
        if ( !$isResponseSuccessful ) {
            throw new Exception( $this->EXCEPTION_MESSAGE );
        }

        // -- Return response
        return $response->getData();
    }

    /**
     * Sets languages from API to cache
     */
    private function setLanguages()
    {
        $appLang = explode('-', Yii::$app->language);
        $languages = $this->getResponse(self::URL_GET_LANGUAGES, [ 'ui' => $appLang[0] ]);
        $languagesArray = [];
        foreach ($languages['langs'] as $langKey => $langName) {
            $languagesArray[$langKey] = [
                'code' => $langKey,
                'name' => $langName,
            ];
        }
        Yii::$app->getCache()->set( self::CACHE_KEY_LANGUAGES, $languagesArray, self::CACHE_DURATION_LANGUAGES );
    }

    /**
     * @return array
     * Array of setted languages
     */
    private function getLanguages()
    {
        $langs = Yii::$app->getCache()->get( self::CACHE_KEY_LANGUAGES );
        if ( empty($langs) || !$langs ) {
            $this->setLanguages();
            $langs = Yii::$app->getCache()->get( self::CACHE_KEY_LANGUAGES );
        }
        return $langs;
    }

    /**
     * @param array|string $language
     * @return bool|array
     * Language array
     */
    private function getLanguage($language)
    {
        $allLanguages = $this->getLanguages();
        if ( is_array($language) ) {
            // -- Get by code
            if ( !empty( $language['code'] ) ) {
                return $allLanguages[ $language['code'] ];
            }
            // -- If first element in array is a language code
            if (
                count($language) == 2 &&
                is_string($language[0]) &&
                in_array(strlen($language), [2, 3]) && ctype_lower($language)
            ) {
                return $allLanguages[ $language[0] ];
            }
        } elseif ( is_string($language) ) {
            // -- If given string is language code
            if ( in_array(strlen($language), [2, 3]) && ctype_lower($language) ) {
                return $allLanguages[ $language ];
            } else { // -- If given string is language name
                foreach ($allLanguages as $langObj) {
                    if ( $langObj['name'] == $language ) {
                        return $langObj;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Returns word translation from cache or from the translator API response
     *
     * @param string $word
     * @param string|array $targetLanguage
     * @param string|array $sourceLanguage
     * @return array
     * Normalized Word translation with elements named 'text', 'sourceLanguage', 'targetLanguage'
     */
    private function getWordTranslate( $word, $targetLanguage, $sourceLanguage )
    {
        // -- Set variables
        $word = strlen($word) > 10000 ? substr($word, 0, 10000) : $word;
        $sourceLanguage = $this->getLanguage($sourceLanguage);
        $targetLanguage = $this->getLanguage($targetLanguage);
        $lngCode = "{$sourceLanguage['code']}-{$targetLanguage['code']}";
        $wordCacheKey = self::CACHE_KEY_TRANSLATE . md5($word);
        $wordTranslate = [];

        // -- Check Cache and set language code
        if ( $cachedTranslate = Yii::$app->getCache()->get($wordCacheKey) ) {
            if ( empty($cachedTranslate[ $lngCode ]) ) {
                $cachedTranslate1stEl = array_values($cachedTranslate)[0];
                $lngCode = "{$cachedTranslate1stEl['sourceLanguage']['code']}-{$targetLanguage['code']}";
            }
        }

        if ( !empty($cachedTranslate[ $lngCode ]) ) {
            // -- Get cached translation
            $wordTranslate = $cachedTranslate[ $lngCode ];
        } else {
            // -- Get translation and set cache
            if ( empty($sourceLanguage) ) {
                // -- If source language is empty
                $translatedText = $this->getResponse(
                    self::URL_TRANSLATE,
                    [ 'text' => $word, 'lang' => $targetLanguage['code'] ]
                );
                $wordTranslate = $this->getNormalizedWordArray(
                    $translatedText['text'],
                    $word,
                    explode('-', $translatedText['lang'])[0],
                    $targetLanguage
                );

            } else {
                // -- If source language is not empty
                $translatedText = $this->getResponse(
                    self::URL_TRANSLATE,
                    [
                        'text' => $word,
                        'lang' => "{$sourceLanguage['code']}-{$targetLanguage['code']}"
                    ]
                );
                $wordTranslate = $this->getNormalizedWordArray(
                    $translatedText['text'],
                    $word,
                    $sourceLanguage,
                    $targetLanguage
                );
            }
            $this->setWordTranslate($wordTranslate);
        }

        return $wordTranslate;
    }

    /**
     * Sets word translation cache
     *
     * @param array $word
     * Normalized Word array with elements named 'text', 'sourceLanguage', 'targetLanguage'
     */
    private function setWordTranslate( $word )
    {
        $lngCode = "{$word['sourceLanguage']['code']}-{$word['targetLanguage']['code']}";
        $wordCacheKey = self::CACHE_KEY_TRANSLATE . md5($word['sourceText']);
        $cachedWord = Yii::$app->getCache()->get($wordCacheKey);
        if ( empty($cachedWord) ) $cachedWord = [];
        $cachedWord[ $lngCode ] = $word;
        Yii::$app->getCache()->set($wordCacheKey, $cachedWord, self::CACHE_DURATION_TRANSLATE);
    }

    /**
     * @param $text
     * @param $sourceText
     * @param $sourceLanguage
     * @param $targetLanguage
     * @return array
     */
    private function getNormalizedWordArray($text, $sourceText, $sourceLanguage, $targetLanguage)
    {
        $text = is_array($text) ? implode('', $text) : $text;
        $sourceLanguage = empty($sourceLanguage['name']) && empty($sourceLanguage['code']) ?
            $this->getLanguage($sourceLanguage) :
            $sourceLanguage;
        $targetLanguage = empty($targetLanguage['name']) && empty($targetLanguage['code']) ?
            $this->getLanguage($targetLanguage) :
            $targetLanguage;
        return [
            'text' => $text,
            'sourceText' => $sourceText,
            'sourceLanguage' => [
                'name' => $sourceLanguage['name'],
                'code' => $sourceLanguage['code']
            ],
            'targetLanguage' => [
                'name' => $targetLanguage['name'],
                'code' => $targetLanguage['code']
            ]
        ];
    }
}