<?php

namespace app\APITranslator;

class APITranslator implements APITranslatorInterface
{
    /**
     * @var APIAbstractTranslator
     */
    private $translator;

    /**
     * APITranslator constructor.
     * @param APIAbstractTranslator $translatorClass
     */
    function __construct( $translatorClass )
    {
        $this->translator = $translatorClass;
    }

    public function isLanguageAutoDetectAvailable()
    {
        return $this->translator->isLanguageAutoDetectAvailable();
    }

    /**
     * @inheritDoc
     */
    public function getAvailableLanguages()
    {
        return $this->translator->getAvailableLanguages();
    }

    /**
     * @inheritDoc
     */
    public function getAvailableLanguagesFor($sourceLanguage)
    {
        return $this->translator->getAvailableLanguagesFor($sourceLanguage);
    }

    /**
     * @inheritDoc
     */
    public function isTranslationAvailable($sourceLanguage, $targetLanguage)
    {
        return $this->translator->isTranslationAvailable($sourceLanguage, $targetLanguage);
    }

    /**
     * @inheritDoc
     */
    public function getTranslation($word, $targetLanguage, $sourceLanguage = '')
    {
        // -- Setting source language
        if ( empty($sourceLanguage) ) {
            if ( !$this->translator->isLanguageAutoDetectAvailable() ) {
                if ( $detectedSourceLanguage = $this->translator->detectLanguage($word) ) {
                    $sourceLanguage = $detectedSourceLanguage;
                } else {
                    return false;
                }
            }
        }

        // -- Setting translation languages
        if ( empty($targetLanguage) ) {
            return false;
        } elseif ( !empty($sourceLanguage) && !empty($targetLanguage) ) {
            if ( !$this->translator->isTranslationAvailable($sourceLanguage, $targetLanguage) ) {
                return false;
            }
        }

        // -- Process translation
        return $this->translator->getTranslation($word, $targetLanguage, $sourceLanguage);
    }

    /**
     * @inheritDoc
     */
    public function getTranslations($word, $targetLanguages = [], $sourceLanguage = '')
    {
        // -- Setting source language
        if ( empty($sourceLanguage) ) {
            if ( !$this->translator->isLanguageAutoDetectAvailable() ) {
                if ( $detectedSourceLanguage = $this->translator->detectLanguage($word) ) {
                    $sourceLanguage = $detectedSourceLanguage;
                } else {
                    return false;
                }
            }
        }

        // -- Setting translation languages
        if ( empty($targetLanguages) && !empty($sourceLanguage) ) {
            $targetLanguages = $this->translator->getAvailableLanguagesFor($sourceLanguage);
        } elseif ( empty($targetLanguages) && empty($sourceLanguage) ) {
            $sourceLanguage = $this->translator->detectLanguage($word);
            $targetLanguages = $this->translator->getAvailableLanguagesFor($sourceLanguage);
        } elseif ( !empty($sourceLanguage) && !empty($targetLanguages) ) {
            $availableTargetLanguages = [];
            foreach ($targetLanguages as $targetLanguage) {
                if ( $this->isTranslationAvailable($sourceLanguage, $targetLanguage) ) {
                    $availableTargetLanguages[] = $targetLanguage;
                }
            }
            if ( empty($availableTargetLanguages) ) {
                return false;
            } else {
                $targetLanguages = $availableTargetLanguages;
            }
        }

        // -- Process translation
        return $this->translator->getTranslations($word, $targetLanguages, $sourceLanguage);
    }

    /**
     * @inheritDoc
     */
    public function detectLanguage($word)
    {
        return $this->translator->detectLanguage($word);
    }
}