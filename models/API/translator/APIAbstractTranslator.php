<?php

namespace app\APITranslator;

use Yii;
use yii\base\Object;
use yii\httpclient\Client;

abstract class APIAbstractTranslator extends Object implements APITranslatorInterface
{
    /**
     * @var array
     * Array of keys for the current translation API
     */
    public $APIKeys = [];

    /**
     * @var bool
     * If current translator adapter can detect the language automatically
     */
    protected $isLanguageAutoDetectAvailable = true;

    /**
     * @var string
     * Message for the Exception trowed by translator adapters
     */
    protected $EXCEPTION_MESSAGE;

    /**
     * @var \yii\httpclient\Client
     */
    protected $client;

    /**
     * @var string
     * Base url of the request server
     */
    protected $baseUrl = '';

    /**
     * @var array
     * Array of available languages for translation
     */
    protected $availableLanguages = [];

    /**
     * @var array
     * Basic configuration of the request
     */
    protected $requestConfig = [
        'format' => Client::FORMAT_JSON
    ];

    /**
     * @var array
     * Basic configuration of the client response
     */
    protected $responseConfig = [
        'format' => Client::FORMAT_JSON
    ];

    /**
     * Sets the http request object
     */
    function init()
    {
        $this->EXCEPTION_MESSAGE = Yii::t('app/api', 'Translation server error!');
        $this->client = new Client([
            'requestConfig' => $this->requestConfig,
            'responseConfig' => $this->responseConfig,
        ]);
        parent::init();
    }

    public function isLanguageAutoDetectAvailable()
    {
        return $this->isLanguageAutoDetectAvailable;
    }

    /**
     * @inheritdoc
     */
    abstract public function getAvailableLanguages();

    /**
     * @inheritdoc
     */
    abstract public function getAvailableLanguagesFor( $sourceLanguage );

    /**
     * @inheritdoc
     */
    abstract public function getTranslation( $word, $targetLanguage, $sourceLanguage );

    /**
     * @inheritdoc
     */
    abstract public function getTranslations( $word, $targetLanguages, $sourceLanguage );

    /**
     * @inheritdoc
     */
    abstract public function detectLanguage( $word );

    /**
     * @inheritDoc
     */
    abstract public function isTranslationAvailable( $sourceLanguage, $targetLanguage );
}