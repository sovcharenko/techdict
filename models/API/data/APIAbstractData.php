<?php

namespace app\APIData;

use Yii;
use yii\base\Object;
use yii\httpclient\Client;

abstract class APIAbstractData extends Object implements APIDataInterface
{
    /**
     * @var \yii\httpclient\Client
     */
    protected $client;

    /**
     * @var string
     * Message for the Exception trowed by translator adapters
     */
    protected $EXCEPTION_MESSAGE;

    /**
     * @var string
     * Base url of the request server
     */
    protected $baseUrl = '';

    /**
     * @var array
     * Basic configuration of the request
     */
    protected $requestConfig = [
        'format' => Client::FORMAT_JSON
    ];

    /**
     * @var array
     * Basic configuration of the client response
     */
    protected $responseConfig = [
        'format' => Client::FORMAT_JSON
    ];

    /**
     * Sets the http request object
     */
    function init()
    {
        $this->EXCEPTION_MESSAGE = Yii::t('app/api', 'Translation server error!');
        $this->client = new Client([
            'baseUrl' => $this->baseUrl,
            'requestConfig' => $this->requestConfig,
            'responseConfig' => $this->responseConfig,
        ]);
        parent::init();
    }

    /**
     * @inheritdoc
     */
    abstract public function getRelatedLinks( $word );

    /**
     * @inheritdoc
     */
    abstract public function getRelatedLinkContent( $linkID );
}