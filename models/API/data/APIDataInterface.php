<?php

namespace app\APIData;

use yii\base\Object;
use yii\httpclient\Client;

interface APIDataInterface
{
    /**
     * @param $word
     * Source word or phrase
     * @return array
     * Array of related data for the source word or phrase
     */
    public function getRelatedLinks( $word );

    /**
     * @param $linkID
     * Url or other identifier of the related data or resource
     * @return string|array
     * Full content of the related data or the resource
     */
    public function getRelatedLinkContent( $linkID );
}