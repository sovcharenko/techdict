<?php

namespace app\APIDataAdapters;

use Yii;
use app\APIData\APIAbstractData;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\httpclient\Exception as ClientException;
use yii\base\Exception;

class WikipediaData extends APIAbstractData
{
    /**
     * Partial request configuration
     */
    const REQUEST_GET_LINKS = [
        'action' => 'query',
        'list' => 'search',
        'srwhat' => 'text',
        'cmlimit' => '30',
    ];
    const REQUEST_GET_LINK_CONTENT = [
        'action' => 'query',
        'prop' => 'revisions',
        'rvprop' => 'content',
        'redirects' => '1',
    ];
    const RESPONSE_FORMAT = [
        'format' => 'json'
    ];

    /**
     * @var array Error codes
     */
    private $APIErrorsCodes = ['404', '401', '402'];

    /**
     * @inheritDoc
     */
    public $baseUrl = 'https://{{lang}}.wikipedia.org/w/api.php';

    /**
     * @inheritDoc
     */
    protected $requestConfig = [
        'format' => Client::FORMAT_URLENCODED
    ];

    /**
     * @inheritDoc
     */
    public function getRelatedLinks($word)
    {
        $relatedLinks = $this->getResponse(self::REQUEST_GET_LINKS, ['srsearch' => $word]);
        return $relatedLinks[ "query" ][ "search" ];
    }

    /**
     * @inheritDoc
     */
    public function getRelatedLinkContent($linkID)
    {
        $linkContent = $this->getResponse(self::REQUEST_GET_LINKS, ['titles' => $linkID]);
        return $linkContent[ "query" ];
    }

    /**
     * *****************************************************************************************************************
     * Internal methods
     * *****************************************************************************************************************
     */

    /**
     * @param array $requestConfig
     * Request params needed to send the request (yeah)
     * @param array $requestData
     * Array of request data, it will be joined with API key
     * @return array|\yii\httpclient\Response->data
     * Returns array - parsed JSON response
     * @throws ClientException
     * @throws Exception
     * When the network is unavailable or response has errors (for example, wrong API key)
     */
    private function getResponse($requestConfig, $requestData)
    {
        $response = [];

        // -- We trying to use app language first, then english (default) if we will have an error
        $appLang = explode('-', Yii::$app->language);
        $responseLangs = [
            $appLang[0],
            'en'
        ];

        for($i = 0; $i < count($responseLangs); $i++) {
            $isRequestSuccessful = true;

            try {
                // -- Request
                $url = str_replace('{{lang}}', $responseLangs[$i], $this->baseUrl);
                $data = ArrayHelper::merge(
                    ArrayHelper::merge(
                        self::RESPONSE_FORMAT,
                        $requestConfig
                    ),
                    ArrayHelper::merge(
                        [
                            'uselang' => $responseLangs[$i],
                            'responselanginfo' => true,
                        ],
                        $requestData
                    )
                );
                $response = $this->client->createRequest()
                    ->setMethod( 'POST' )
                    ->setUrl( $url )
                    ->setData( $data )
                    ->send();
            } catch (Exception $clientException) {
                // -- Handle errors
                if ($clientException instanceof ClientException) {
                    // -- If we have a problem with network or API
                    Yii::warning(
                        $clientException->getCode() . $clientException->getMessage() . PHP_EOL .
                        $clientException->getTraceAsString() . PHP_EOL
                    );
                    throw new Exception( $this->EXCEPTION_MESSAGE );
                } else {
                    // -- If we have another problem, generally - with parsing, s
                    // -- so response is incorrect for current language
                    Yii::warning(
                        $clientException->getCode() . $clientException->getMessage() . PHP_EOL .
                        $clientException->getTraceAsString() . PHP_EOL
                    );
                    $isRequestSuccessful = false;
                }
            }

            // -- If everything is OK
            if ($isRequestSuccessful) {
                break;
            }

            // -- If we have errors on all languages
            if ( !$isRequestSuccessful && !isset($responseLangs[$i + 1]) ) {
                throw new Exception( $this->EXCEPTION_MESSAGE );
            }
        }

        // -- Check response codes
        if (
            in_array($response->getStatusCode(), $this->APIErrorsCodes) ||
            empty($response)
        ) {
            Yii::warning( $this::className() . " {$response->data['code']} {$response->data['message']}");
            throw new Exception( $this->EXCEPTION_MESSAGE );
        }

        // -- Return response
        return $response->getData();
    }
}