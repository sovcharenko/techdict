<?php

namespace app\APIData;

class APIData implements APIDataInterface
{
    /**
     * @var APIAbstractData
     */
    private $dataAPI;

    /**
     * APIData constructor.
     * @param APIAbstractData $dataAPIClass
     */
    function __construct( $dataAPIClass )
    {
        $this->dataAPI = $dataAPIClass;
    }

    /**
     * @inheritDoc
     */
    public function getRelatedLinks($word)
    {
        return $this->dataAPI->getRelatedLinks($word);
    }

    /**
     * @inheritDoc
     */
    public function getRelatedLinkContent($linkID)
    {
        return $this->dataAPI->getRelatedLinkContent($linkID);
    }
}