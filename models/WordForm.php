<?php

namespace app\models;

use Yii;
use app\APIData\APIData;
use app\APIDataAdapters\WikipediaData;
use app\APITranslator\APITranslator;
use app\APITranslatorAdapters\YandexTranslator;
use yii\base\Exception;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * ContactForm is the model behind the contact form.
 */
class WordForm extends Model
{
    public $text;
    public $sourceLanguage;
    public $targetLanguage;
    public $translations;
    public $meaning_links;
    public $is_public;

    /**
     * @var APITranslator
     */
    private $APITranslator;

    /**
     * @var APIData
     */
    private $APIData;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->APITranslator = new APITranslator( new YandexTranslator() );
        $this->APIData = new APIData( new WikipediaData() );
        parent::init();
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // -- Validation
            [['text', 'sourceLanguage', 'targetLanguage'], 'required'],
            [['text'], 'string'],
            [['is_public'], 'safe'],
            [['sourceLanguage'], 'in', 'range' => array_keys( $this->getLanguages() ) ],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'text' => Yii::t('app/models', 'Text'),
            'sourceLanguage' => Yii::t('app/models', 'Source Language'),
            'targetLanguage' => Yii::t('app/models', 'Target Language'),
            'translations' => Yii::t('app/models', 'Translations'),
            'meaning_links' => Yii::t('app/models', 'Meaning Links'),
            'is_public' => Yii::t('app/models', 'Is Public'),
        ];
    }

    /**
     * @return array
     * Of available languages
     */
    public function getLanguages()
    {
        return $this->APITranslator->isLanguageAutoDetectAvailable() ?
            ArrayHelper::merge(
                ['auto' => ['code' => 'auto', 'name' => Yii::t('app/models', 'Try to detect language automatically') ]],
                $this->APITranslator->getAvailableLanguages()
            ) :
            $this->APITranslator->getAvailableLanguages();
    }

    /**
     * @return array
     * Of available languages
     */
    public function getTargetLanguages()
    {
        return $this->APITranslator->getAvailableLanguages();
    }

    /**
     * Makes requests to data and translator APIs.
     * Returns true if requests was successful and writes responses to own props ($translations and $meaning_links).
     * Returns false if something goes wrong.
     *
     * @return bool
     */
    public function getTranslate()
    {
        if ( $this->validate() ) {
            $isRequestSuccessful = true;

            try {
                $this->translations = $this->APITranslator->getTranslations(
                    $this->text,
                    $this->targetLanguage,
                    (empty($this->sourceLanguage) || $this->sourceLanguage == 'auto' ? null : $this->sourceLanguage)
                );
            } catch (Exception $translatorException) {
                $this->addError('text', $translatorException->getMessage());
                $isRequestSuccessful = false;
            }

            try {
                $this->meaning_links = $this->APIData->getRelatedLinks( $this->text );
            } catch (Exception $dataApiException) {
                $this->addError('text', $dataApiException->getMessage());
                $isRequestSuccessful = false;
            }

            if ( $isRequestSuccessful ) {
                return true;
            }
        }

        return false;
    }

    public function save()
    {
        
    }
}
