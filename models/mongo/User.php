<?php

namespace app\models\mongo;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for collection "user".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $username
 * @property mixed $password
 * @property mixed $auth_key
 * @property mixed $access_token
 * @property mixed $role
 */
class User extends \yii\mongodb\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * Default user password
     * @var string
     */
    private static $default_password = '741852963';

    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['techdict', 'user'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'name',
            'password',
            'auth_key',
            'access_token',
            'role',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password', 'auth_key', 'access_token', 'role'], 'safe'],
            [['username', 'password', 'auth_key', 'access_token'], 'string'],
            [['username', 'role'], 'required'],
            [['password'], 'default', 'value' => Yii::$app->security->generatePasswordHash( self::$default_password ), 'when' => function($model) { return $model->isNewRecord; }],
            [['access_token', 'auth_key'], 'default', 'value' => Yii::$app->security->generateRandomString(), 'when' => function($model) { return $model->isNewRecord; }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('app\models', 'ID'),
            'username' => Yii::t('app\models', 'Name'),
            'password' => Yii::t('app\models', 'Password'),
            'auth_key' => Yii::t('app\models', 'Auth Key'),
            'access_token' => Yii::t('app\models', 'Access Token'),
            'role' => Yii::t('app\models', 'Role'),
        ];
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::find()->where(['username' => $username])->one();
    }

    /**
     * Identity Interface **********************************************************************************************
     */

    /**
     * @inheritDoc
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * @inheritDoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::find()->where(['access_token' => $token])->one();
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @inheritDoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritDoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }
}
