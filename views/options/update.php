<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DL\Options */

$this->title = Yii::t('app/views', 'Update {modelClass}: ', [
    'modelClass' => 'Options',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Options'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app/views', 'Update');
?>
<div class="options-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
