<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\editable\Editable;
use app\models\DL\User;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DL\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app/views', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app/views', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'responsive'=>true,
        'hover'=>true,
        'resizableColumns'=>true,
//        'showPageSummary' => true,

        'toolbar' => [
            [
                'content'=>
                    Html::button('<i class="glyphicon glyphicon-plus"></i>', [
                        'type'=>'button',
                        'title'=>Yii::t('app/views', 'Create User'),
                        'class'=>'btn btn-success'
                    ]) . ' '.
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], [
                        'class' => 'btn btn-default'
                    ]),
            ],
            '{export}',
            '{toggleData}'
        ],

        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            //'id',
            [
                'class'=>'kartik\grid\EditableColumn',
                'attribute'=>'username',
                'vAlign'=>'middle',
                'editableOptions'=> ['formOptions' => ['action' => ['/user/fastedit']]]
            ],
            //'password',
            //'auth_key',
            //'access_token',
            [
                'class'=>'kartik\grid\EditableColumn',
                'attribute'=>'role',
                'vAlign'=>'middle',

                'filterType' => GridView::FILTER_SELECT2,

                'editableOptions'=> [
                    'formOptions' => ['action' => ['/user/fastedit']],
                    'asPopover' => true,
                    'inputType' => Editable::INPUT_DROPDOWN_LIST,
                    'data' => User::roles(),
                    'displayValueConfig'=> User::roles(),
                ]
            ],

            ['class' => 'kartik\grid\ActionColumn'],
        ],
    ]); ?>
</div>
