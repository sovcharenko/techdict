<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\DL\User;

/* @var $this yii\web\View */
/* @var $model app\models\DL\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'role')->radioList( User::roles(), [
        'class' => 'btn-group',
        'data-toggle' => 'buttons',
        'item' => function ($index, $label, $name, $checked, $value) {
            return '<label class="btn btn-default' . ($checked ? ' active' : '') . '">' .
            Html::radio($name, $checked, ['value' => $value]) . $label . '</label>';
        },
    ] ) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app/views', 'Create') : Yii::t('app/views', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
