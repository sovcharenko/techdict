<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DL\Word */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="word-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'word')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'source_language')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'translations')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'meaning_links')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'is_public')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app/views', 'Create') : Yii::t('app/views', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
