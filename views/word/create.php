<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\WordForm */
/* @var $form yii\widgets\ActiveForm */
/* @var $data_received bool */

$this->title = Yii::t('app/views', 'Create Word');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Words'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="word-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="word-form">

        <?php $form = ActiveForm::begin(); ?>

        <div class="clearfix">
            <div class="col-sm-6">
                <?= $form->field($model, 'sourceLanguage')->widget(\kartik\select2\Select2::className(), [
                    'data' => ArrayHelper::map($model->getLanguages(), 'code', 'name'),
                    'options' => ['placeholder' => Yii::t('app/views', 'Select source Language')],
                ]) ?>
                <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>
            </div>

            <div class="col-sm-6">
                <?= $form->field($model, 'targetLanguage')->widget(\kartik\select2\Select2::className(), [
                    'data' => ArrayHelper::map($model->getTargetLanguages(), 'code', 'name'),
                    'options' => ['placeholder' => Yii::t('app/views', 'Select target Language'),],
                    'pluginOptions' => ['multiple' => true],
                ]) ?>
                <?= $form->field($model, 'is_public')->checkbox() ?>
            </div>
        </div>

        <?php if ( $data_received ) : ?>
            <div class="clearfix">
                <pre>
                    <?php var_dump( $model->translations ) ?>
                </pre>
                <p></p>
                <pre>
                    <?php var_dump( $model->meaning_links ) ?>
                </pre>
            </div>
        <?php endif; ?>

<!--        --><?//= $form->field($model, 'translations')->textarea(['rows' => 6]) ?>
<!---->
<!--        --><?//= $form->field($model, 'meaning_links')->textarea(['rows' => 6]) ?>

        <div class="form-group col-sm-12">
            <?= Html::submitButton(Yii::t('app/views', 'Create'), ['class' => 'btn btn-success pull-right']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
