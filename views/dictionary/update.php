<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DL\Dictionary */

$this->title = Yii::t('app/views', 'Update {modelClass}: ', [
    'modelClass' => 'Dictionary',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/views', 'Dictionaries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app/views', 'Update');
?>
<div class="dictionary-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
