<?php

/* @var $this yii\web\View */

$this->title = 'TechDict';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><br><br>
            Congratulations!
        </h1>

        <p class="lead">Now You could work with the <b>TechDict</b></p>

        <p><a class="btn btn-lg btn-success" href="<?= (Yii::$app->user->isGuest ? '/login' : '/word') ?>">Get started</a></p>

    </div>

    <div class="body-content">

    </div>
</div>
