<?php

use yii\db\Migration;

class m170407_104639_rbac extends Migration
{
    /**
     * @var array of roles
     *
     * 'assign' contains array of a conditions that we could use in QueryBuilder->...->where([...]) statement
     */
    private static $roles = [
        'guest' => [
            'description' => 'Nobody',
            'parent' => 'user',
            'assign' => []
        ],
        'user' => [
            'description' => 'Can work with words and dictionaries',
            'parent' => 'admin',
            'assign' => [
                ['role' => 'user']
            ]
        ],
        'admin' => [
            'description' => 'Can do anything including managing users',
            'parent' => '',
            'assign' => [
                ['role' => 'admin']
            ]
        ],
    ];

    public function up()
    {
        $rbac = Yii::$app->getAuthManager();

        // -- Creating RBAC instances
        if ( ($this->db->driverName === 'mysql') && !empty( $rbac->db ) && $rbac->db == 'db' )
            $this->execute( file_get_contents( Yii::getAlias('@yii/rbac/schema-mysql.sql') ) );

        // -- Add roles
        $role_objects = [];
        foreach ( self::$roles as $role_name => $role ) {
            $new_role = $rbac->createRole($role_name);
            $new_role->description = $role['description'];
            $rbac->add($new_role);
        }

        // -- Add roles hierarchy
        foreach ( self::$roles as $role_name => $role ) {
            $child_role = $rbac->getRole($role_name);
            $parent_role = $rbac->getRole($role['parent']);
            if ($parent_role) $rbac->addChild($parent_role, $child_role);
        }

        // -- Assign roles to users with specific params
        foreach ( self::$roles as $role_name => $role ) {
            if (!empty($role['assign'])) {
                $current_role = $rbac->getRole($role_name);
                foreach ($role['assign'] as $assign_condition) {
                    $users = (new yii\db\Query())->select(['id', 'username'])
                        ->from('user')
                        ->where($assign_condition)
                        ->all();
                    foreach ($users as $user) $rbac->assign($current_role, $user['id']);
                }
            }
        }

    }

    public function down()
    {
        $rbac = Yii::$app->getAuthManager();
        $rbac->removeAll();
    }
}
