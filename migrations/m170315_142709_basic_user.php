<?php

use yii\db\Migration;

class m170315_142709_basic_user extends Migration
{
    // -- Users Credentials
    private static $users = [
        'admin' => [
            'username' => 'Admin',
            'password' => '8524567913',
            'role_id' => 'admin',
        ],
        'sample_user' => [
            'username' => 'SampleUser',
            'password' => '123456',
            'role_id' => 'user',
        ]
    ];

    public function up()
    {
        // -- Insert Admin
        $this->insert('user', [
            'username' => self::$users['admin']['username'],
            'password' => Yii::$app->security->generatePasswordHash( self::$users['admin']['password'] ),
            'auth_key' => Yii::$app->security->generateRandomString(),
            'access_token' => Yii::$app->security->generateRandomString(),
            'role' => self::$users['admin']['role_id'],
        ]);

        // -- Insert Sample User
        $this->insert('user', [
            'username' => self::$users['sample_user']['username'],
            'password' => Yii::$app->security->generatePasswordHash( self::$users['sample_user']['password'] ),
            'auth_key' => Yii::$app->security->generateRandomString(),
            'access_token' => Yii::$app->security->generateRandomString(),
            'role' => self::$users['sample_user']['role_id'],
        ]);
    }

    public function down()
    {
    }
}
