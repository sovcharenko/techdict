<?php

use yii\db\Migration;

class m170315_140239_basic_tables extends Migration
{
    public function up()
    {
        // -- Creating tables
        $tableOptions = ($this->db->driverName === 'mysql') ? 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB' : null;

        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string(45),
            'password' => $this->string(100),
            'auth_key' => $this->string(100),
            'access_token' => $this->string(100),
            'role' => $this->string(10),
        ], $tableOptions);

        $this->createTable('word', [
            'id' => $this->primaryKey(),
            'word' => $this->text(),
            'source_language' => $this->string(120),
            'translations' => $this->text(),
            'meaning_links' => $this->text(),
            'is_public' => $this->boolean(),
        ], $tableOptions);

        $this->createTable('dictionary', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'title' => $this->text(),
            'description' => $this->text(),
            'is_public' => $this->boolean(),
        ], $tableOptions);

        $this->createTable('dictionary_has_word', [
            'id' => $this->primaryKey(),
            'dictionary_id' => $this->integer(),
            'word_id' => $this->integer(),
            'user_note' => $this->text(),
        ], $tableOptions);

        $this->createTable('options', [
            'id' => $this->primaryKey(),
            'key' => $this->string(30),
            'title' => $this->string(50),
            'value' => $this->text(),
        ], $tableOptions);
 
        // -- Creating foreign keys
        $this->addForeignKey('fk-dictionary-user_id', 'dictionary', 'user_id', 'user', 'id');
        $this->addForeignKey('fk-dictionary_has_word-dictionary_id', 'dictionary_has_word', 'dictionary_id', 'dictionary', 'id');
        $this->addForeignKey('fk-dictionary_has_word-word_id', 'dictionary_has_word', 'word_id', 'word', 'id');
    }

    public function safeDown() // -- safeDown just in case something goes wrong
    {
        // -- Drop foreign keys
        $this->dropForeignKey('fk-dictionary-user_id ', 'dictionary');
        $this->dropForeignKey('fk-dictionary_has_word-dictionary_id', 'dictionary_has_word');
        $this->dropForeignKey('fk-dictionary_has_word-word_id', 'dictionary_has_word');

        // -- Truncate tables
        $this->truncateTable('dictionary_has_word');
        $this->truncateTable('word');
        $this->truncateTable('dictionary');
        $this->truncateTable('user');
        $this->truncateTable('options');

        // -- Delete tables
        $this->dropTable('dictionary_has_word');
        $this->dropTable('word');
        $this->dropTable('dictionary');
        $this->dropTable('user');
        $this->dropTable('options');
    }
}
