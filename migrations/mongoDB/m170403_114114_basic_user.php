<?php

class m170403_114114_basic_user extends \yii\mongodb\Migration
{
    // -- Admin Credentials
    private $user_name = 'admin';
    private $user_password = '8524567913';
    private $user_role_id = 0;

    public function up()
    {
        echo PHP_EOL . 'User data:' . PHP_EOL;
        echo 'username - ' . $this->user_name . PHP_EOL;
        echo 'password_hash - ' . Yii::$app->security->generatePasswordHash( $this->user_password ) . PHP_EOL;
        echo 'auth_key - ' . Yii::$app->security->generateRandomString() . PHP_EOL;
        echo 'access_token - ' . Yii::$app->security->generateRandomString() . PHP_EOL;
        echo 'role - ' . $this->user_role_id . PHP_EOL . PHP_EOL;

        $this->insert('user', [
            'username' => $this->user_name,
            'password' => Yii::$app->security->generatePasswordHash( $this->user_password ),
            'auth_key' => Yii::$app->security->generateRandomString(),
            'access_token' => Yii::$app->security->generateRandomString(),
            'role' => $this->user_role_id,
        ]);


    }

    public function down()
    {

    }
}
