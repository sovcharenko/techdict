<?php

class m170326_115407_basic_tables extends \yii\mongodb\Migration
{
    public $predefined_collections = [
        // RBAC
        'auth_item' => [
            'indexes' => [
                ['name' => 1]
            ]
        ],
        'auth_assignment' => [
            'indexes' => [
                ['item_name' => 1],
                ['user_id' => 1]
            ]
        ],
        'auth_rule' => [
            'indexes' => [
                ['name' => 1]
            ]
        ],
        // User
        'user' => [
            'indexes' => [
                ['username' => 1],
                ['role' => 1],
            ]
        ],
        // Words
        'word' => [
            'indexes' => [
                ['word' => 1],
                ['source_language' => 1],
                ['is_public' => 1]
            ]
        ],
        'dictionary' => [
            'indexes' => [
                ['user_id' => 1],
                ['is_public' => 1],
            ]
        ],
        'dictionary_has_word' => [
            'indexes' => [
                ['word_id' => 1],
                ['dictionary_id' => 1],
            ]
        ],
        // Options
        'options' => [
            'indexes' => [
                ['key' => 1]
            ]
        ],
    ];

    public function up()
    {
        foreach ($this->predefined_collections as $name => $opts) {
            try {
                $this->createCollection($name);
            } catch (\yii\mongodb\Exception $e) {
                echo $e->getMessage() . PHP_EOL;
            }

            if (!empty($opts['indexes'])) {
                foreach ($opts['indexes'] as $index) {
                    try {
                        $this->createIndex($name, $index);
                    } catch (\yii\mongodb\Exception $e) {
                        echo $e->getMessage() . PHP_EOL;
                    }
                }
            }
        }
    }

    public function down()
    {
        foreach ($this->predefined_collections as $name => $opts) {
            try {
                $this->dropCollection($name);
            } catch (\yii\mongodb\Exception $e) {
                echo $e->getMessage();
            }
        }
    }
}
