<?php

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'components' => [
        'cache' => [
            'class' => 'yii\redis\Cache',
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => 0,
        ],
        // -- RBAC
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest'],
//            'class' => 'sweelix\rbac\redis\Manager',
//            'db' => 'redis',
//            'defaultRoles' => ['guest'],
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => ( YII_DEBUG ? require(__DIR__ . '/db-local.php') : require(__DIR__ . '/db.php') ),
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/components/translation',
                    'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app'        => 'app.php',
                        'app/errors' => 'errors.php',
                        'app/models' => 'models.php',
                        'app/views'  => 'views.php',
                    ],
                ],
            ],
        ],

//        'mongodb' => require(__DIR__ . '/mongo-db.php'),
    ],
    'params' => ( YII_DEBUG ? require(__DIR__ . '/params-local.php') : require(__DIR__ . '/params.php') ),
//    'controllerMap' => [
//        'mongodb-migrate' => require(__DIR__ . '/mongo-db-migrate.php')
//    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
