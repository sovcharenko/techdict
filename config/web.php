<?php

$config = [
    'id' => 'TechDict',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'components' => [

        // --
        // -- System Settings
        // --

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true, // set 'useFileTransport' to false and configure a transport mailer to send real emails
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        // -- Cache (Redis)
        'cache' => [
            'class' => YII_DEBUG ? 'yii\redis\Cache' : 'yii\caching\FileCache',
        ],

        // -- RBAC
        'authManager' => YII_DEBUG ? [
            'class' => 'sweelix\rbac\redis\Manager',
            'db' => 'redis',
            'defaultRoles' => ['guest'],
        ] : [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest'],
        ],
        'user' => [
            'identityClass' => 'app\models\DL\User',
            'enableAutoLogin' => true,
        ],

        // -- Url/Request manager (pretty Url's)
        'request' => [
            'cookieValidationKey' => 'jsHprvfkzvia5HrnFEnFQnnU0xCkz9KN',
            'baseUrl' => '',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
            ],
        ],

        // -- Translation of the website elements
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/components/translation',
                    'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app'        => 'app.php',
                        'app/errors' => 'errors.php',
                        'app/models' => 'models.php',
                        'app/views'  => 'views.php',
                    ],
                ],
            ],
        ],

        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
                ],
            ],
        ],

        'assetManager' => [
            'bundles' => [
                'dmstr\web\AdminLteAsset' => [
                    'skin' => 'skin-black',
                ],
            ],
        ],

        // --
        // -- Database
        // --

        // -- MySQL
        'db' => ( YII_DEBUG ? require(__DIR__ . '/db-local.php') : require(__DIR__ . '/db.php') ),

        // -- MongoDB
        //'mongodb' => require(__DIR__ . '/mongo-db.php'),

    ],

    // -- Custom namespaces
    'aliases' => [
        '@app/APITranslator' => str_replace(['/', '\\'], DS, dirname(__DIR__) . '/models/API/translator'),
        '@app/APITranslatorAdapters' => str_replace(['/', '\\'], DS, dirname(__DIR__) . '/models/API/translator/adapters'),
        '@app/APIData' => str_replace(['/', '\\'], DS, dirname(__DIR__) . '/models/API/data'),
        '@app/APIDataAdapters' => str_replace(['/', '\\'], DS, dirname(__DIR__) . '/models/API/data/adapters'),
        '@app/assets' => str_replace(['/', '\\'], DS, dirname(__DIR__) . '/components/assets'),
    ],

    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module',
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            'downloadAction' => 'gridview/export/download',
            'i18n' => [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@kvgrid/messages',
                'forceTranslation' => true
            ]
        ]
    ],

    // -- System Params
    'params' => ( YII_DEBUG ? require(__DIR__ . '/params-local.php') : require(__DIR__ . '/params.php') ),
];

// -- Redis
if (YII_DEBUG) $config['components']['redis'] = [
    'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => 0,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
//    $config['modules']['gii']['generators'] = [
//        'mongoDbModel' => [
//            'class' => 'yii\mongodb\gii\model\Generator'
//        ]
//    ];
}

return $config;
