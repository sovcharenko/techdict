<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=rzcsyney_techdict',
    'username' => 'rzcsyney_techdic',
    'password' => '8524567913',
    'charset' => 'utf8',
    
    // -- Caching
    'enableSchemaCache' => true,
    'schemaCacheDuration' => 3600*24*365, // one year
    'schemaCache' => 'cache',

    'queryCache' => 'cache',
    'queryCacheDuration' => 3600*24*7, // one week
];
