<?php

return [
    'class' => 'yii\mongodb\console\controllers\MigrateController',
//    'migrationNamespaces' => [
//        'app\migrations\mongoDB',
//    ],
    'migrationPath' => 'migrations\mongoDB', // allows to disable not namespaced migration completely
];